import { initializeApp } from 'firebase/app';
import { getFirestore } from '@firebase/firestore';
import { GoogleAuthProvider } from '@firebase/auth';
import 'firebase/firestore';
import 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyA3oYHZAQBZcIZaa2AVThYfjLs0UTpvRb4",
    authDomain: "journalapp-c7410.firebaseapp.com",
    projectId: "journalapp-c7410",
    storageBucket: "journalapp-c7410.appspot.com",
    messagingSenderId: "927814219741",
    appId: "1:927814219741:web:b692d973cacfefb0402834"
};

initializeApp(firebaseConfig);

const db = getFirestore();
const googleAuthProvider = new GoogleAuthProvider();

export {
    db,
    googleAuthProvider
}