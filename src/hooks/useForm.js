import { useState } from "react";

const useForm = (state = {}) => {
    const [formValues, setFormValues] = useState(state);

    const handlerInputForm = (e) => {
        e.preventDefault();

        setFormValues({
            ...formValues,
            [e.target.name]: e.target.value
        })
    }

    return [formValues, handlerInputForm];
}

export { useForm };