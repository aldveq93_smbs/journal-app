import React from 'react'
import { Link } from 'react-router-dom';

const Register = () => {
    return (
        <>
            <h3 className='auth__title'>Register</h3>

            <form action="">
                <input className='auth__input' type="text" placeholder='Name' name='name' />
                <input className='auth__input' type="password" placeholder='Password' name='password' />
                <input className='auth__input' type="password" placeholder='Confirm password' name='confirmPassword' />
                <button type='submit' className='btn btn-primary btn-block pointer mb-5'>Register</button>
                <Link to='/auth/login' className='link'>Already registered?</Link>
            </form>
        </>
    )
}

export default Register;