import React from 'react'
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { loginWithEmailPass, startGoogleLogin } from '../actions/auth';
import { useForm } from '../hooks/useForm';

const Login = () => {

    const [formValues, handlerInputForm] = useForm({email: 'aldveq80@gmail.com', password: 1234});
    const { email, password } = formValues;

    const dispatch = useDispatch();

    const handlerFormSubmit = (e) => {
        e.preventDefault();
        dispatch(loginWithEmailPass(email, password));
    }

    const handlerGoogleLogin = () => {
        dispatch(startGoogleLogin());
    }
    
    return (
        <>
            <h3 className='auth__title'>Login</h3>

            <form onSubmit={handlerFormSubmit}>
                <input className='auth__input' type="text" placeholder='Email' name='email' value={email} onChange={handlerInputForm} />
                <input className='auth__input' type="password" placeholder='Password' name='password' value={password} onChange={handlerInputForm} />
                <button type='submit' className='btn btn-primary btn-block pointer'>Login</button>
                <div className='auth__social-networks'>
                    <p>Login with socials networks</p>
                    <div className="google-btn mb-5" onClick={handlerGoogleLogin}>
                        <div className="google-icon-wrapper">
                            <img className="google-icon" src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg" alt="google button" />
                        </div>
                        <p className="btn-text">
                            <b>Sign in with google</b>
                        </p>
                    </div>

                    <Link to='/auth/register' className='link'>Create new account</Link>
                </div>
            </form>
        </>
    )
}

export default Login;