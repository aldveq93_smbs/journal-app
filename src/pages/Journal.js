import React from 'react'
import { Notes, Sidebar } from '../components';

const Journal = () => {
    return (
        <div className='journal__main-wrapper'>
            <Sidebar />

            <main className='journal__main-content'>
                {/* <Feedback /> */}
                <Notes />
            </main>
        </div>
    )
}

export default Journal;