import React from 'react'

const Feedback = () => {
    return (
        <div className='feedback-container'>
            <p className="feedback-container__desc">Please, select a note or create an entry!</p>
            <br />
            <i className="far fa-star feedback-container__icon"></i>
        </div>
    )
}

export default Feedback;