import React from 'react'
import Note from '../Note';

const Notes = () => {
    return (
        <div className='notes__main-content'>
            <Note />
            <div className='notes__content'>
                <input type="text" placeholder='Title' className='notes__title-input' />
                <textarea placeholder='What happened today?' className='notes__textarea'></textarea>
                <div className='notes__image'>
                    <img src="https://images.unsplash.com/photo-1501362343565-93d7318347cf?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Nnx8aG9yaXpvbnxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80" alt="placeholder" />
                </div>
            </div>
        </div>
    )
}

export default Notes;