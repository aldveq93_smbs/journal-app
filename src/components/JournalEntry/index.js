import React from 'react'

const JournalEntry = () => {
    return (
        <div className='journal__entry pointer'>
            <div className='journal__entry-picture' style={{backgroundImage: 'url(https://media.revistagq.com/photos/5ca602e93efb23a987379002/3:2/w_645,h_430,c_limit/que_significa_la_palabra_random_3683.jpg)'}}></div>

            <div className='journal__entry-body'>
                <p className='journal__entry-title'>A new day!</p>
                <p className='journal__entry-content'>lorem ipsum content</p>
            </div>

            <div className='journal__entry-date-box'>
                <span>Monday</span>
                <h4>28</h4>
            </div>
        </div>
    )
}

export default JournalEntry;