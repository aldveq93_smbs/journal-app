import React from 'react'
import { JournalEntry } from '..';

const JournalEntries = () => {

    const getEntries = () => {
        const entries = [1,2,3,4,5];
        return entries.map(entry => <JournalEntry key={entry} />);
    }

    return (
        <div className='journal__entries'>
            {getEntries()}
        </div>
    )
}

export default JournalEntries;