import { types } from "../action-types/actionTypes";
import { getAuth, signInWithPopup } from "@firebase/auth";
import { googleAuthProvider } from "../firebase/firebase-config";

const loginWithEmailPass = () => {
    return (dispatch) => {
        setTimeout(() => {
            dispatch(login(12345, 'Aldo')); 
        }, 2000);
    }
}

const startGoogleLogin = () => {
    return (dispatch) => {
        const auth = getAuth();
        signInWithPopup(auth, googleAuthProvider)
        .then(({user}) => {
            dispatch(login(user.uid, user.displayName));
        });
    }
}

const login = (uid, name) => ({
    type: types.login,
    payload: {
        uid,
        name
    }
})

export {loginWithEmailPass, startGoogleLogin};